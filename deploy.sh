#!/bin/bash

set -e

cd manifest

envsubst < deployment.yaml > main-deployments.yaml
kubectl apply -f namespace.yaml
kubectl apply -f main-deployments.yaml
kubectl apply -f ingress.yaml
kubectl apply -f services.yaml
